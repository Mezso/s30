const express = require('express')
const mongoose = require('mongoose')
const port = 4000;

const app = express();

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// mongodb+srv://admin:<password>@zuittbootcamp.5mphj.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

mongoose.connect('mongodb+srv://admin:car5412364@zuittbootcamp.5mphj.mongodb.net/session30?retryWrites=true&w=majority', 
{
	useNewUrlParser:true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

	//console.error.bind(console)- print error in the browser and in the terminal
	db.on("error",console.error.bind(console,"Connection Error"))
	// if the connection is successful, this will be the output in our console.
	db.once('open',() => console.log('Connected to the cloud database'))


// Mongoose schema


const taskSchema = new mongoose.Schema({
	//  define the name of our scema - taskSchema
	//  new mongoose.Schema method to make a schema
	//  we will be needing the name of the task and its status
	//  each field will require a data type

	name: String,
	status:{
		type: String,
		default: 'pending'
	}
})


const Task = mongoose.model('Task',taskSchema)
	// models use schemas and they act as the middleman from the server to our database
    // Model can now be used to run commands for interacting with our database.
    // Naming convetion - name of model should be capitalized and singular form - 'Task' 
    // second parameter is used to specify the scema of the documents that will be stored in the mongDB collection

// business logic

	/*
		1. Add a functionality to check if there are duplicate tasks
			-if the task already exist in the db, we return an error
			-if the test doesn't exist in the db, we add it in the db.
		2. The task data will be coming from the request body.
		3. Create a new Task object with name field property


	*/

app.post('/task',(req,res) =>{
		Task.findOne({name: req.body.name},(err,result) =>{
			if(result != null && result.name === req.body.name){
				return res.send('Duplicate task found.')
			}
			else {
				let newTask = new Task({
					name : req.body.name
				})

				newTask.save((savedErr,savedTask)=>{
					if(savedErr){
						return console.error(savedErr)
					}
					else{
						return res.status(201).send('New Task Created')
					}
				})
			}
		})
})


app.get('/task',(req,res)=> {
	Task.find({},(err, result) =>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

// Acitivty

const playerSchema = new mongoose.Schema({

	username: String,
	password: String
})

const Player = mongoose.model('Player',playerSchema)

app.post('/signup',(req,res) =>{
	Player.findOne({name: req.body.username},(err,result)=>{
		if(result != null && result.username === req.body.username){
			return res.send('Duplicate username found please Try again')
		}
		else{
			let newPlayer = new Player({
				username: req.body.username,
				password: req.body.password

			})
			newPlayer.save((saveErr,savePlayer)=>{
					if(saveErr){
						return console.error(saveErr)
					}
					else{
						return res.status(201).send('Your Account Created')
					}
				})
		}
	})
})
app.get('/users',(req,res)=> {
	Player.find({},(err, result) =>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server is running at port ${port}`))